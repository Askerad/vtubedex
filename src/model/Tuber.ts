export default interface Tuber {
    id: string;
    name: string;
    twitchUserId?: string;
    stream?: any;
    profile?: any;
}
