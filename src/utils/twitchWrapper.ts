import axios from "axios";
import qs from "qs";
import Tuber from "../model/Tuber";

type AxiosMethods = "get" | "post" | "patch" | "delete" | "put";

export default class TwitchWrapper {
    private secret: String;
    private clientId: String = "xpptrtzkva6yjtqv3t0tzmkkar68dy";
    private bearer: String = "";
    private isBearerReady: Boolean = false;

    constructor() {
        this.secret = "3tz93i5psrscm4hnrkcrzkuhkgk1eq";
    }

    async GetBearerToken() {
        return axios
            .post(
                "https://id.twitch.tv/oauth2/token",
                qs.stringify({
                    client_id: "xpptrtzkva6yjtqv3t0tzmkkar68dy",
                    client_secret: "3tz93i5psrscm4hnrkcrzkuhkgk1eq", // TODO: Gerer comment on renseigne le token PLEASE
                    grant_type: "client_credentials",
                })
            )
            .then((response) => {
                this.bearer = response.data.access_token;
                console.log("bearer : ", this.bearer);
                this.isBearerReady = true;
            })
            .catch((error) => {
                console.error(error);
                // TODO: Implementer un vrai systeme de gestion des plantages
            });
    }

    /**
     * Effectue un call sur l'API de twitch
     * @param uri L'URI de la requete a effectuer, préfixée d'un "/"
     * @param method la methode HTTP de la requete a effectuer
     */
    async Call(uri: String, method: AxiosMethods = "get", data: any) {
        if (!this.isBearerReady)
            throw new Error("Bearer Token not Initialised");
        console.log(uri);
        return axios(`https://api.twitch.tv/helix${uri}`, {
            method,
            headers: {
                Authorization: `Bearer ${this.bearer}`,
                "Client-Id": this.clientId,
            },
            data,
        });
    }

    async GetProfiles(users: Tuber[]) {
        const userParams = users
            .map((user) => `login=${user.twitchUserId}`)
            .join("&");
        return this.Call(`/users?${userParams}`, "get", null);
    }
}
