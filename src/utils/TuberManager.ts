import Tuber from "../model/Tuber";
import TwitchWrapper from "./TwitchWrapper";
import Surreal from "surrealdb.js";

export default class TuberManager {
    tubers: Tuber[] = [];
    twitch: TwitchWrapper;
    db: Surreal;

    constructor(wrapper: TwitchWrapper, db: Surreal) {
        this.twitch = wrapper;
        this.db = db;
    }

    async LoadTubers() {
        this.tubers = await this.db.select("tubers");

        this.parseTwitchUsernames();
    }

    private parseTwitchUsernames() {
        this.tubers = this.tubers.map((tuber: any) => {
            tuber.twitchUserId = tuber.id.split(":")[1];
            return tuber;
        });
    }

    GetTwitchUsernames() {
        return this.tubers.map((tuber: any) => tuber.twitchUserId);
    }

    async GetTwitchStreamInfo() {
        const twitch_stream_response = await this.twitch.Call(
            `/streams?${this.GetTwitchUsernames()
                .map((tuber: any) => `user_login=${tuber}`)
                .join("&")}`,
            "get",
            null
        );
        const streams = twitch_stream_response.data.data;

        const twitch_profile_response = await this.twitch.GetProfiles(
            this.tubers
        );
        const profiles = twitch_profile_response.data.data;

        this.tubers.forEach((tuber, index) => {
            const stream = streams.find(
                (stream: any) => tuber.twitchUserId === stream.user_login
            );
            const profile = profiles.find(
                (profile: any) => tuber.twitchUserId === profile.login
            );

            if (stream) {
                stream.thumb_md = stream.thumbnail_url
                    .replace(/\{width\}/, "1280")
                    .replace(/\{height\}/, "720");

                const now = Date.now();
                const start = new Date(stream.started_at);
                stream.uptime = new Date(now - start.getTime())
                    .toISOString()
                    .slice(11, -1)
                    .slice(0, 5);

                this.tubers[index].stream = stream;
            }

            this.tubers[index].profile = profile;
        });
    }

    GetOnlineChannels() {
        return this.tubers
            .filter((tuber: any) => tuber.stream)
            .sort((tuberA: Tuber, tuberB: Tuber) => {
                return tuberA.stream.uptime > tuberB.stream.uptime ? 1 : -1;
            });
    }

    GetOfflineChannels() {
        return this.tubers.filter((tuber: any) => !tuber.stream);
    }

    static Match(tuber: Tuber, value: string): boolean {
        const regex = new RegExp(value, "ig");

        return (
            regex.test(tuber.name) ||
            regex.test(tuber.twitchUserId) ||
            regex.test(tuber.stream?.game_name)
        );
    }
}
