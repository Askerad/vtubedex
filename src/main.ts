import { createApp } from "vue";
import "./style.css";
import App from "./App.vue";
import { createWebHashHistory, createRouter } from "vue-router";
import Home from "./pages/Home.vue";
import axios from "axios";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import TwitchWrapper from "./utils/twitchWrapper";

library.add(fas);

const routes = [
    {
        path: "/",
        component: Home,
    },
];

const app = createApp(App);

app.use(
    createRouter({
        history: createWebHashHistory(),
        routes,
    })
);
app.component("faIcon", FontAwesomeIcon);
app.provide("TwitchWrapper", new TwitchWrapper());

app.mount("#app");
